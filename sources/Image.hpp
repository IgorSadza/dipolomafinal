/*
    Image.hpp
    Purpose: Load image from file.

    @author Igor Sadza
    @version 0.1 - 07/03/19
*/
#ifndef IMAGE_HPP
#define IMAGE_HPP

#include "CommonIncludes.hpp"

class Image {
    private:
        std::string     m_imageName;
        glm::ivec2      m_imageSize;
        GLubyte         *m_imagePixels;        
        GLuint          m_imageID;
    public:
        Image();
        Image(const std::string &t_pathToImage);

        std::string     getName();
        glm::ivec2&     getSize();   
        GLubyte*        getPixels(); 
        
        GLvoid          useImage();

};

#endif //IMAGE_HPP