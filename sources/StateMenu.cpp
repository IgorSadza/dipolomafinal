/*
    StateMenu.hpp
    Purpose: Menu state

    @author Igor Sadza
    @version 0.1 - 07/03/19
*/
#include "StateMenu.hpp"

static StateMenu *stateMenu;

StateMenu::StateMenu(const std::string &t_pathToStateSource) {
  stateMenu = this;
  State::Register("StateMenu", stateMenu);
  State::setInstance("StateMenu");
}

GLvoid StateMenu::stateLogic() {
}

GLvoid StateMenu::stateRender() {}