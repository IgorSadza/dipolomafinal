#include "StatePlay.hpp"

static StatePlay *statePlay;

StatePlay::StatePlay(const std::string &t_pathToStateSource) {
  statePlay = this;
  State::Register("StatePlay", statePlay);
  State::setInstance("StatePlay");
}

GLvoid StatePlay::stateLogic() {
}

GLvoid StatePlay::stateRender() {}