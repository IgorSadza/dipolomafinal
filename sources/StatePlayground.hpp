/*
    StatePlayground.hpp
    Purpose: State playground for testing.

    @author Igor Sadza
    @version 0.1 - 07/03/19
*/
#ifndef STATE_PLAYGROUND_HPP
#define STATE_PLAYGROUND_HPP

#include "CommonIncludes.hpp"
#include "State.hpp"

class StatePlayground :public State{
private:
public:
  StatePlayground();
  GLvoid stateLogic();
  GLvoid stateRender();
};

#endif // STATE_PALYGROUND_HPP