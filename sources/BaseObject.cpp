/*
    BaseObject.cpp
    Purpose: State playground for testing.

    @author Igor Sadza
    @version 0.1 - 07/03/19
*/
#include "BaseObject.hpp"
#include "PrimitiveRenderer.hpp"
#include "TextRenderer.hpp"
#include "ResourceManager.hpp"

BaseObject::BaseObject(){}

GLvoid BaseObject::setImage(const std::string &t_imageFile) {
  (!ResourceManager<Image>::searchObject(t_imageFile))
  ? m_image = Image(t_imageFile)
  : m_image = ResourceManager<Image>::getObject(t_imageFile);

  m_primitive.getSize() = m_image.getSize();
}

GLvoid BaseObject::setShader(const std::string &t_shaderFile) {
  (!ResourceManager<Shader>::searchObject(t_shaderFile))
  ? m_shader = Shader(t_shaderFile)
  : m_shader = ResourceManager<Shader>::getObject(t_shaderFile);

}

GLvoid BaseObject::setText(const std::string &t_textToRender, 
                       const std::string &t_fontFile, 
                       GLuint t_fontSize) {

 m_text.setFont(t_fontFile, t_fontSize);
 m_text.setPrimitive(m_primitive);
 m_text.setText(t_textToRender);
}

Image &BaseObject::getImage()         { return m_image; }

Shader &BaseObject::getShader()       { return m_shader; }

Primitive &BaseObject::getPrimitive() { return m_primitive; }

Text &BaseObject::getText()           { return m_text; }

GLvoid BaseObject::renderObject() {
    
  m_shader.useShader();
  m_image.useImage();

  glm::mat4 modelMatrix;
  modelMatrix = glm::translate(modelMatrix, glm::vec3(m_primitive.getPosition(), 0.0f));
  modelMatrix = glm::scale(modelMatrix, glm::vec3(m_primitive.getSize(), 0.0f));

  m_shader.sendVariable("t_model", modelMatrix);
  m_shader.sendVariable("t_color", m_primitive.getColor());

  PrimitiveRenderer::renderPrimitive();

  m_text.renderText();
}

GLvoid BaseObject::loadObject() {
    *this = *m_savedSettings;
}

GLvoid BaseObject::saveObject() {
  m_savedSettings = new BaseObject();
  *m_savedSettings = *this;
}