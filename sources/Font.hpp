/*
    FontManager.hpp
    Purpose: Font manager.

    @author Igor Sadza
    @version 0.1 - 08/03/19
*/
#ifndef FONT_HPP
#define FONT_HPP

#include "CommonIncludes.hpp"

struct Glyph {
private:
  GLuint        m_glyphID;
  glm::ivec2    m_glyphSize;
  glm::ivec2    m_glyphBearing;
  GLint64       m_glpyhAdvance;

public:
  Glyph(GLuint      t_glyphID, 
        glm::ivec2  t_glyphSize, 
        glm::ivec2  t_glyphBearing, 
        GLint64     t_glpyhAdvance);

  GLuint        getGlyphID();
  glm::ivec2    getGlyphSize();
  glm::ivec2    getGlyphBearing();
  GLint64       getGlyphAdvance();
};

class Font {
private:
  typedef std::map<GLchar, Glyph*> GlyphRegistry;

  GlyphRegistry m_glyphRegistry;
  std::string   m_fontName;
  FT_Library    m_library;
  FT_Face       m_face;

  GLvoid            initFont();

public:
  Font();
  Font(const std::string &t_pathToFontFile, GLuint t_fontSize);

  GlyphRegistry     getFont();
  std::string       getName();
};

#endif //FONT_MANAGER_HPP