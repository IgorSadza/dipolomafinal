/*
    State.hpp
    Purpose: State.

    @author Igor Sadza
    @version 0.1 - 07/03/19
*/
#include "State.hpp"

State *State::m_instance = 0;
State::StateRegister State::m_registry;

State *State::GetInstance() { return m_instance; }

GLvoid State::Register(const std::string &_name, State *_state) {
  m_registry.insert(StateRegister::value_type(_name, _state));
}

GLvoid State::setInstance(const std::string &_name) {
  m_instance = LookUp(_name);
}

State *State::LookUp(const std::string &_name) {
  for (auto &ite : m_registry) {
    if (ite.first == _name) {
      return ite.second;
    }
  }
}