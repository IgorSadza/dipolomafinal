/*
    CursorObject.hpp
    Purpose: Cursor manager.

    @author Igor Sadza
    @version 0.1 - 08/03/19
*/
#ifndef CURSOR_OBJECT_HPP
#define CURSOR_OBJECT_HPP

#include "CommonIncludes.hpp"
#include "BaseObject.hpp"

class CursorObject {
    private:
        CursorObject();
        static  BaseObject      m_cursorBaseObject;
    public:

        static CursorObject&    init();
        static BaseObject       &getObject();
        static GLvoid           useCursor(GLboolean t_currsorHide = false);

};

#endif //CURSOR_OBJECT_HPP