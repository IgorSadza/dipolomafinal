/*
    Image.cpp
    Purpose: Load image from file.

    @author Igor Sadza
    @version 0.1 - 07/03/19
*/
#include "Image.hpp"
#include "ResourceManager.hpp"
#include "FileParser.hpp"

Image::Image() {}

Image::Image(const std::string &t_pathToImage) {
    
  m_imagePixels = stbi_load(t_pathToImage.c_str(), &m_imageSize.x, &m_imageSize.y, nullptr, 0);

  if (!m_imagePixels) {
    std::cout << "The sprite cannot be open: " + t_pathToImage << std::endl;
    exit (EXIT_FAILURE);       
  }

  m_imageName = FileParser::getParsedName(t_pathToImage);

  glGenTextures(1, &m_imageID);
  glBindTexture(GL_TEXTURE_2D, m_imageID);

  glPixelStorei(GL_UNPACK_ROW_LENGTH, m_imageSize.x);
  glPixelStorei(GL_UNPACK_IMAGE_HEIGHT, m_imageSize.y);

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

  glGenerateMipmap(GL_TEXTURE_2D);

  glTexImage2D(
  GL_TEXTURE_2D, 
  0, 
  GL_RGBA, 
  m_imageSize.x, 
  m_imageSize.y, 
  0, 
  GL_RGBA,
  GL_UNSIGNED_BYTE, 
  m_imagePixels
  );

  glBindTexture(GL_TEXTURE_2D, 0);
  ResourceManager<Image>::registerObject(*this);
}

std::string Image::getName()    { return m_imageName; }
glm::ivec2 &Image::getSize()    { return m_imageSize; }
GLubyte *Image::getPixels()     { return m_imagePixels; }

GLvoid Image::useImage() { glBindTexture(GL_TEXTURE_2D, m_imageID); }





