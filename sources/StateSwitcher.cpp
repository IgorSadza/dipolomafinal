/*
    StateSwticher.cpp
    Purpose: State switcher.

    @author Igor Sadza
    @version 0.1 - 07/03/19
*/
#include "StateSwitcher.hpp"
#include "StatePlayground.hpp"

StateSwitcher::StateSwitcher() {
    //StateMenu *stateMenu = new StateMenu("resources/states/menu.state");
    StatePlayground *statePlayground = new StatePlayground();
}

StateSwitcher &StateSwitcher::init() {
    static StateSwitcher stateSwitcher;
    return stateSwitcher;
}

GLvoid StateSwitcher::selectedStateLogic() {
    State::GetInstance()->stateLogic();
}

GLvoid StateSwitcher::selectedStateRender() {
    State::GetInstance()->stateRender();
}