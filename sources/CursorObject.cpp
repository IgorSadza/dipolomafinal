/*
    CursorObject.cpp
    Purpose: Cursor manager.

    @author Igor Sadza
    @version 0.1 - 08/03/19
*/
#include "CursorObject.hpp"
#include "SharedResources.hpp"
#include "Image.hpp"

BaseObject CursorObject::m_cursorBaseObject;

CursorObject::CursorObject() {
    
    m_cursorBaseObject.setImage("resources/sprites/cursors/cursor_normal.png");
    
    GLFWcursor  *glfwCursor;
    GLFWimage   *glfwImage = new GLFWimage();

    glfwImage->pixels   = m_cursorBaseObject.getImage().getPixels();
    glfwImage->height   = m_cursorBaseObject.getImage().getSize().x;
    glfwImage->width    = m_cursorBaseObject.getImage().getSize().y;

    glfwCursor = glfwCreateCursor(glfwImage, 0, 0);

    glfwSetCursor(SharedResources::window, glfwCursor);

    m_cursorBaseObject.getPrimitive().setPosition(-50.0f);
    m_cursorBaseObject.getPrimitive().setSize(10.0f);
    m_cursorBaseObject.getPrimitive().setColor(1.0f);
}

BaseObject &CursorObject::getObject() {
    return m_cursorBaseObject;
}

CursorObject &CursorObject::init() {
    static CursorObject cursorObject;
    return cursorObject;
}

GLvoid CursorObject::useCursor(GLboolean t_currsorHide) {

    (t_currsorHide)
    ? glfwSetInputMode(SharedResources::window, GLFW_CURSOR, GLFW_CURSOR_NORMAL)
    : glfwSetInputMode(SharedResources::window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
}