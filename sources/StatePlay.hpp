/*
    StatePlay.hpp
    Purpose: Menu state

    @author Igor Sadza
    @version 0.1 - 07/03/19
*/
#ifndef STATE_PLAY_HPP
#define STATE_PLAY_HPP

#include "CommonIncludes.hpp"
#include "State.hpp"

class StatePlay : public State {
private:
public:
  StatePlay(const std::string &t_pathToStateSource);
  GLvoid stateLogic();
  GLvoid stateRender();
};

#endif // STATE_PLAY_HPP