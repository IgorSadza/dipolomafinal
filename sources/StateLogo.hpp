/*
    StateLogo.hpp
    Purpose: -

    @author Igor Sadza
    @version 0.1 - 07/03/19
*/
#ifndef STATE_LOGO_HPP
#define STATE_LOGO_HPP

#include "CommonIncludes.hpp"
#include "State.hpp"

class StateLogo : public State {
private:
public:
  StateLogo(const std::string &t_pathToStateSource);
  GLvoid stateLogic();
  GLvoid stateRender();
};

#endif // STATE_LOGO_HPP