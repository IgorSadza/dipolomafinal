/*
    Rectangle.hpp
    Purpose: Set rectangle for primitive.

    @author Igor Sadza
    @version 0.1 - 07/03/19
*/
#ifndef PRIMITIVE_HPP
#define PRIMITIVE_HPP

#include "CommonIncludes.hpp"

class Primitive {
private:

  template<class Type>
  struct Implementation {
    template <class Arg>
    static Type parsePointers(const Arg &t_arg, int t_argSize);

    template <class... Args> 
    static Type createVector(Args... t_args);
  };

  glm::vec2 m_size;
  glm::vec2 m_position;
  glm::vec4 m_color;

public:
  // Constructor.
  Primitive() : m_size(10.0f), m_position(0.0f), m_color(1.0f) {}

  // Seters.
  template <class... Args> 
  void setSize(Args... t_args);
  glm::vec2 &getSize() { return m_size; }

  template <class... Args> 
  void setPosition(Args... t_args);
  glm::vec2 &getPosition() { return m_position; }

  template <class... Args> 
  void setColor(Args... t_args);
  glm::vec4 &getColor() { return m_color; }
};

static GLboolean checkPrimitiveCollision(Primitive t_one, Primitive t_two) {

    bool collisionX = t_one.getPosition().x + t_one.getSize().x >= t_two.getPosition().x &&
                      t_two.getPosition().x + t_two.getSize().x >= t_one.getPosition().x;

    bool collisionY = t_one.getPosition().y + t_one.getSize().y >= t_two.getPosition().y &&
                      t_two.getPosition().y + t_two.getSize().y >= t_one.getPosition().y;

    return collisionX && collisionY;
}


#include "Primitive.hxx"


#endif //PRIMITIVE_HPP