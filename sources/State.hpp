/*
    State.hpp
    Purpose: State.

    @author Igor Sadza
    @version 0.1 - 07/03/19
*/
#ifndef STATE_HPP
#define STATE_HPP

#include "CommonIncludes.hpp"

class State {
private:
  typedef std::map<std::string, State*> StateRegister;
  static StateRegister m_registry;
  static State *m_instance;

public:
  static State *GetInstance();
  static GLvoid Register(const std::string &_name, State *_state);
  static GLvoid setInstance(const std::string &_name);

  virtual GLvoid stateRender() = 0;
  virtual GLvoid stateLogic()  = 0;

protected:
  static State *LookUp(const std::string &_name);
};

#endif //STATE_HPP