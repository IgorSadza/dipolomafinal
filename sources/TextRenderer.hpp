/*
    TextRenderer.hpp
    Purpose: Render text.

    @author Igor Sadza
    @version 0.1 - 10/03/19
*/
#ifndef TEXT_RENDERER_HPP
#define TEXT_RENDERER_HPP

#include "CommonIncludes.hpp"
#include "Shader.hpp"
#include "Text.hpp"

class TextRenderer {
    private:
        TextRenderer();
        static GLuint           m_vao;
        static GLuint           m_vbo;
        static Shader           m_textShader;
    public:
        static TextRenderer     &init();
        static GLvoid           renderText(Text &t_textObject);
};

#endif //TEXT_RENDERER_HPP