/*
    StateLogo.hpp
    Purpose: -

    @author Igor Sadza
    @version 0.1 - 07/03/19
*/
#include "StateLogo.hpp"

static StateLogo *stateLogo;

StateLogo::StateLogo(const std::string &t_pathToStateSource) {
  stateLogo = this;
  State::Register("StateLogo", stateLogo);
  State::setInstance("StateLogo");
}

GLvoid StateLogo::stateLogic() {}

GLvoid StateLogo::stateRender() {}