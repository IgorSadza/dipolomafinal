/*
    main.cpp
    Purpose: Main file.

    @author Igor Sadza
    @version 0.1 - 07/03/19
*/
#include "CommonIncludes.hpp"
#include "SharedResources.hpp"
#include "Settings.hpp"

#include "StateSwitcher.hpp"
#include "CursorObject.hpp"

namespace Input {
GLvoid mousePosition(GLFWwindow *t_window, GLdouble t_posX, GLdouble t_posY);
}

int main(int argc, char const *argv[]) {

// Create window and set witch OpenGL version we use.  
glfwInit();
glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);

SharedResources::window = glfwCreateWindow(Settings::windowWidth, Settings::windowHeight, Settings::windowName, NULL, NULL);
glfwMakeContextCurrent(SharedResources::window);    

// Callback function.
glfwSetCursorPosCallback(SharedResources::window, Input::mousePosition);

// Load OpenGL functions.
gladLoadGLLoader(GLADloadproc(glfwGetProcAddress));
glViewport(0, 0, Settings::windowWidth, Settings::windowHeight);

// Blending functions.
glEnable(GL_BLEND);
glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

// Init singleton classes
StateSwitcher::init();
CursorObject::init();
// Game loop.
GLfloat lastFrame = 0.0f;
while (!glfwWindowShouldClose(SharedResources::window)) {

    GLfloat currentFrame = glfwGetTime();
    SharedResources::deltaTime = currentFrame - lastFrame;
    lastFrame = currentFrame;

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);
    
    StateSwitcher::selectedStateLogic();
    StateSwitcher::selectedStateRender();
    
    glfwSwapBuffers(SharedResources::window);
    glfwPollEvents();
}
// Terminate window.
glfwTerminate(); 
return 0;
}


namespace Input {
void mousePosition(GLFWwindow *t_window, double t_posX, double t_posY) {
    CursorObject::getObject().getPrimitive().setPosition((float)t_posX, (float)t_posY);
}
}