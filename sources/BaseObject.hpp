/*
    BaseObject.hpp
    Purpose: Create object thath handle shader, fonts, image, rectangle 
             and send this variables to specyfic renderers. 

    @author Igor Sadza
    @version 0.1 - 07/03/19
*/
#ifndef BASE_OBJECT_HPP
#define BASE_OBJECT_HPP

#include "CommonIncludes.hpp"
#include "Primitive.hpp"
#include "Shader.hpp"
#include "Image.hpp"
#include "Text.hpp"

class BaseObject {
    private:
        Image       m_image;
        Shader      m_shader;
        Text        m_text;
        Primitive   m_primitive;        

        BaseObject  *m_savedSettings;        
    public:
        BaseObject();

        GLvoid setImage     (const std::string &t_imageFile);
        GLvoid setShader    (const std::string &t_shaderFile);
        GLvoid setText      (const std::string &t_textToRender, 
                             const std::string &t_fontFile = "resources/fonts/04b_30.ttf", 
                             GLuint t_fontSize = 15);

        Image       &getImage();
        Shader      &getShader();        
        Primitive   &getPrimitive();
        Text        &getText();

        GLvoid saveObject();
        GLvoid loadObject();
        GLvoid renderObject();
};

#endif // BASE_OBJECT_HPP