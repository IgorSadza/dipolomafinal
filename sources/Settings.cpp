/*
    CommonIncludes.hpp
    Purpose: File for global settings.

    @author Igor Sadza
    @version 0.1 - 06/03/19
*/

#include "Settings.hpp"

namespace Settings {
const GLchar *windowName      = "Praca Dyplomowa Igor Sadza";
const GLfloat windowWidth     = 800.0f;
const GLfloat windowHeight    = 600.0f;
} // namespace Settings
