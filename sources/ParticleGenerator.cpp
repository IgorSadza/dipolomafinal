/*
    ParticleGenerator.hpp
    Purpose: Particle generator.

    @author Igor Sadza
    @version 0.1 - 08/03/19
*/
#include "ParticleGenerator.hpp"
#include "ResourceManager.hpp"
#include "PrimitiveRenderer.hpp"
#include "SharedResources.hpp"

// ! Partcile class.

Particle::Particle() : Primitive(), m_life(0.0f), m_velocity(1.0f) {}

GLvoid Particle::setLife(GLfloat t_life) { 
    m_life = t_life; 
}

GLvoid Particle::setVelocity(GLfloat t_velocity) { 
    m_velocity = t_velocity; 
}

GLfloat &Particle::getLife() { 
    return m_life; 
}

GLfloat &Particle::getVelocity() { 
    return m_velocity; 
}

// ! ParticleGeneratorSettings class.

GLvoid ParticleGeneratorSettings::setImage(const std::string &t_imagePath) {
 (ResourceManager<Image>::searchObject(t_imagePath))
 ? m_image = ResourceManager<Image>::getObject(t_imagePath)
 : m_image = Image(t_imagePath);
}

GLvoid ParticleGeneratorSettings::setShader(const std::string &t_shaderPath) {
 (ResourceManager<Shader>::searchObject(t_shaderPath))
 ? m_shader = ResourceManager<Shader>::getObject(t_shaderPath)
 : m_shader = Shader(t_shaderPath);    
}

GLvoid ParticleGeneratorSettings::setColorRange(glm::vec3 t_colorRange) {
  m_colorRange = t_colorRange;
}

GLvoid ParticleGeneratorSettings::setPositionRange(glm::vec2 &t_positionRange) {
  m_positionRange = &t_positionRange;
}

GLvoid ParticleGeneratorSettings::setPositionRange(glm::vec2 t_positionRange) {
  m_positionRange = new glm::vec2();
  *m_positionRange = t_positionRange;
}

GLvoid ParticleGeneratorSettings::setSizeRange(glm::vec2 t_sizeRange) {
  m_sizeRange = t_sizeRange;
}

glm::vec3 ParticleGeneratorSettings::getColorRange() { 
  return m_colorRange; 
}

glm::vec2 ParticleGeneratorSettings::getPositionRange() {
  return *m_positionRange;
}

glm::vec2 ParticleGeneratorSettings::getSizeRange() { 
  return m_sizeRange; 
}

GLboolean ParticleGeneratorSettings::getBlendingOption() {
  return m_blendingOption;
}

Shader ParticleGeneratorSettings::getShader() { 
  return m_shader; 
}

// ! ParticleGenerator class

ParticleGenerator::ParticleGenerator(GLuint t_particleAmount) {
    for (int i = 0; i < t_particleAmount; i++) {
        Particle particle;
        m_particles.push_back(particle);
    }
}

ParticleGeneratorSettings &ParticleGenerator::getGeneratorSettings() {
    return m_generatorSettings;
}

GLvoid ParticleGenerator::randomiseParticles() {

    for (auto& ite: m_particles) {
        if (ite.getLife() <= 0.0f) {

            for (int i = 0; i < 3; i++) {
                (m_generatorSettings.getColorRange()[i] == 1.0f)
                ? ite.getColor()[i] = 0.5 + ((rand() % 100) / 100.0f)    
                : ite.getColor()[i] = ((rand() % 30) / 100.0f); 
            }

            for (int i = 0; i < 2; i++) {
                std::cout << m_generatorSettings.getPositionRange().y << std::endl;
                (m_generatorSettings.getPositionRange()[i] > 0.0f)
                ? ite.getPosition()[i] = rand() % (GLint)m_generatorSettings.getPositionRange()[i]
                : ite.getPosition()[i] = -1.0f*(rand() % (GLuint)(-1.0f * m_generatorSettings.getPositionRange()[i]));
            }

            ite.setSize(0.1f + ((rand() % 100) / 100.0f));
            ite.setVelocity(0.1 + ((rand() % 100) / 100.0f));
            ite.setLife(1.0f);
        }
    }
    
}

GLvoid ParticleGenerator::updateParticles() {
    for (auto& ite: m_particles) {
        ite.getPosition().y += 0.25 + ite.getVelocity() + SharedResources::deltaTime;
        ite.getColor().w -= 0.15 * ite.getVelocity() * SharedResources::deltaTime;
       //ite.getLife() -= ite.getVelocity() + SharedResources::deltaTime;
    }
}

GLvoid ParticleGenerator::renderParticles() {

    randomiseParticles();
    updateParticles();

    if(m_generatorSettings.getBlendingOption()) {
        glBlendFunc(GL_SRC_ALPHA, GL_ONE);       
    }

    for (auto& ite: m_particles) {

        glm::mat4 modelMatrix;
        modelMatrix = glm::translate(modelMatrix, glm::vec3(ite.getPosition(), 0.0f));
        modelMatrix = glm::scale(modelMatrix, glm::vec3(ite.getSize(), 0.0f));

        m_generatorSettings.getShader().sendVariable("t_model", modelMatrix);
        m_generatorSettings.getShader().sendVariable("t_color", ite.getColor());

        PrimitiveRenderer::renderPrimitive();   
    }

    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

// ParticleGenerator::ParticleGenerator(BaseObject *t_baseObject, ParticleEffect t_particleEffect) {
//     m_particleEffect = t_particleEffect;
//     m_baseObject = t_baseObject;
//     for (int i = 0; i < 500; i++) {
//         Particle particle;
//         m_particles.push_back(particle);
//     }
//     randomiseParticles();
// }


// GLvoid ParticleGenerator::randomiseParticles() {

//     for (auto& ite: m_particles) {
//         glm::vec4   randColor = glm::vec4(1.0f);
//         glm::vec2   randSize = m_baseObject->getPrimitive().getSize();
//         glm::vec2   randPos;
//         GLfloat     randVelocity;

//         if (m_particleEffect == ParticleEffect::RAINFALL) {
//             if (ite.getLife() <= 0.0f || ite.getPosition().y > Settings::windowHeight) {
//                 for (int i = 0; i < 3; i++) {
//                     (m_baseObject->getPrimitive().getColor()[i] == 1.0f)
//                     ? randColor[i] = 0.5 + ((rand() % 100) / 100.0f)    
//                     : randColor[i] = ((rand() % 30) / 100.0f); 
//                 }

//                 for (int i = 0; i < 2; i++) {
//                     if (m_baseObject->getPrimitive().getPosition()[i] > 0.0f) {
//                         randPos[i] = rand() % (GLint)m_baseObject->getPrimitive().getPosition()[i];
//                     } else {
//                         randPos[i] = -1.0f*(rand() % (GLuint)(-1.0f * m_baseObject->getPrimitive().getPosition()[i]));
//                     }
//                 }
//                 std::cout << randSize.x << " " << randSize.y << std::endl;

//                 GLfloat scale = 0.1f + ((rand() % 100) / 100.0f);
//                 randSize *= scale;

//                 randVelocity = 0.1 + ((rand() % 100) / 100.0f);

//                 ite.setSize(randSize); 
//                 ite.setColor(randColor);
//                 ite.setPosition(randPos);
//                 ite.setVelocity(randVelocity);
//                 ite.setLife(1.0f);
//             }
//         } else if (m_particleEffect == ParticleEffect::FOLLOW) {
//             if (ite.getLife() <= 0.0f) {
//                 // for (int i = 0; i < 3; i++) {
//                 //     (m_baseObject->getPrimitive().getColor()[i] == 1.0f)
//                 //     ? randColor[i] = 0.5 + ((rand() % 100) / 100.0f)    
//                 //     : randColor[i] = ((rand() % 30) / 100.0f); 
//                 // }

//                 // GLfloat scale = 0.1f + ((rand() % 100) / 100.0f);
//                 // randSize *= scale;

//                 randVelocity = 0.1 + ((rand() % 100) / 100.0f);

//                 ite.setSize(10.0f); 
//                 ite.setColor(1.0f);
//                 ite.setPosition(m_baseObject->getPrimitive().getPosition());
//                 ite.setVelocity(randVelocity);
//                 ite.setLife(1.0f);                
//             }
//         }
//     }
// }

// GLvoid ParticleGenerator::updateParticles() {

//     for (auto& ite: m_particles) {
//         if (m_particleEffect == ParticleEffect::RAINFALL) {
//             ite.getPosition().y += 0.25 + ite.getVelocity() + SharedResources::deltaTime;
//             ite.getColor().w -= 0.15 * ite.getVelocity() * SharedResources::deltaTime;
//         }else
//         if (m_particleEffect == ParticleEffect::FOLLOW) {
//             ite.getPosition().y -= 0.25 - ite.getVelocity() + SharedResources::deltaTime;             
//             ite.getLife() -= ite.getVelocity() + SharedResources::deltaTime;
//         }
//     }
// }


// GLvoid ParticleGenerator::renderParticles(GLboolean t_blending) {

//   updateParticles();
//   randomiseParticles();

//   m_baseObject->getShader().useShader();
//   m_baseObject->getImage().useImage();

//   glBlendFunc(GL_SRC_ALPHA, GL_CONSTANT_COLOR);      

//   for (auto &ite : m_particles) {

//     glm::mat4 modelMatrix;
//     modelMatrix = glm::translate(modelMatrix, glm::vec3(ite.getPosition(), 0.0f));
//     modelMatrix = glm::scale(modelMatrix, glm::vec3(ite.getSize(), 0.0f));

//     m_baseObject->getShader().sendVariable("t_model", modelMatrix);
//     m_baseObject->getShader().sendVariable("t_color", ite.getColor());

//     PrimitiveRenderer::renderPrimitive();
//   }
  
//   glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
// }