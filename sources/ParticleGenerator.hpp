/*
    ParticleGenerator.hpp
    Purpose: Particle generator.

    @author Igor Sadza
    @version 0.1 - 08/03/19
*/
#ifndef PARTICLE_GENERATOR_HPP
#define PARTICLE_GENERATOR_HPP

#include "CommonIncludes.hpp"
#include "BaseObject.hpp"

class Particle : public Primitive {
private:
  GLfloat m_life;
  GLfloat m_velocity;

public:
  Particle();

  GLvoid setLife(GLfloat t_life);
  GLvoid setVelocity(GLfloat t_velocity);

  GLfloat &getLife();
  GLfloat &getVelocity();
};

class ParticleGeneratorSettings {
private:
  Image m_image;
  Shader m_shader;

  glm::vec3 m_colorRange;
  glm::vec2 *m_positionRange;
  glm::vec2 m_sizeRange;

  GLboolean m_blendingOption;

public:
  GLvoid setImage(const std::string &t_imagePath);
  GLvoid setShader(const std::string &t_shaderPath);

  GLvoid setColorRange(glm::vec3 t_colorRange);
  GLvoid setPositionRange(glm::vec2 &t_positionRange);
  GLvoid setPositionRange(glm::vec2 t_positionRange);
  GLvoid setSizeRange(glm::vec2 t_sizeRange);
  GLvoid setBlendingOptions(GLboolean t_blendingOption);

  glm::vec3 getColorRange();
  glm::vec2 getPositionRange();
  glm::vec2 getSizeRange();
  Shader getShader();
  GLboolean getBlendingOption();
};

class ParticleGenerator {
    private:
        std::vector<Particle>       m_particles;
        ParticleGeneratorSettings   m_generatorSettings;

        GLvoid randomiseParticles();
        GLvoid updateParticles();
    public:
        ParticleGenerator(GLuint t_particleAmount = 500);
        ParticleGeneratorSettings &getGeneratorSettings();
        GLvoid renderParticles();
};

// class ParticleGenerator {
//     private:
//         std::vector<Particle> m_particles;
//         BaseObject            *m_baseObject;
        
//         GLvoid randomiseParticles();
//         GLvoid updateParticles();        
//     public:
//         ParticleGenerator(BaseObject *t_baseObject);
//         GLvoid renderParticles(GLboolean t_blending = false);
// };

#endif //SCENE_PARTICLE_GENERATOR_HPP