/*
    Text.cpp
    Purpose: Text struct for render purpose.

    @author Igor Sadza
    @version 0.1 - 10/03/19
*/
#include "Text.hpp"
#include "ResourceManager.hpp"
#include "TextRenderer.hpp"

Text::Text() {}

GLvoid Text::setPrimitive(Primitive &t_primitive) {
  m_primitive = &t_primitive;
}

GLvoid Text::setFont(const std::string &t_fontPath, GLuint t_fontSize) {
  (!ResourceManager<Font>::searchObject(t_fontPath))
      ? m_font = Font(t_fontPath, t_fontSize)
      : m_font = ResourceManager<Font>::getObject(t_fontPath);
}

GLvoid Text::setText(const std::string t_text) { m_text = t_text; textCentering(); }

std::string &Text::getText() { return m_text; }

Font &Text::getFont() { return m_font; }

glm::vec2 &Text::getPosition() { return m_textPosition; }

Primitive Text::getPrimitive() { return *m_primitive; }

GLvoid Text::renderText() { TextRenderer::renderText(*this); }

GLvoid Text::textCentering() {

  float lineLenght;
  std::string::const_iterator ite;
  for (ite = m_text.begin(); ite != m_text.end(); ite++) {
    Glyph glyph = *m_font.getFont()[*ite];
    lineLenght += (glyph.getGlyphAdvance() >> 6);
  }

  glm::vec2 textRectangle;
  textRectangle.x = lineLenght;
  textRectangle.y = m_font.getFont()['H']->getGlyphSize().y;

  m_textPosition.x = m_primitive->getPosition().x +
                     m_primitive->getSize().x / 2 - textRectangle.x / 2;

  m_textPosition.y = m_primitive->getPosition().y +
                     m_primitive->getSize().y / 2 - textRectangle.y / 2;

  m_textPosition + m_primitive->getPosition();
}