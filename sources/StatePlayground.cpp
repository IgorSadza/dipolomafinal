/*
    StatePlayground.cpp
    Purpose: State playground for testing.

    @author Igor Sadza
    @version 0.1 - 07/03/19
*/
#include "StatePlayground.hpp"

static StatePlayground *statePlayground;

//! #include "FileParser.hpp"
//! #include "Image.hpp"
//! #include "Shader.hpp"
//! #include "Text.hpp"
//! #include "Font.hpp"
//! #include "Primitive.hpp"
#include "PrimitiveRenderer.hpp"
//!#include "TextRenderer.hpp"
//!#include "BaseObject.hpp"
#include "ParticleGenerator.hpp"
#include "CursorObject.hpp"

ParticleGenerator particleGenerator;

StatePlayground::StatePlayground() {
  statePlayground = this;
  State::Register("StatePlayground", statePlayground);
  State::setInstance("StatePlayground");

/*//! Example: FileParser usage.
  TextFileParser tfp("resources/states/play.state");
  for (auto& ite: tfp.getSections()) {
    for (auto& it: tfp.getParasedVariables(ite.m_source)) {
      std::cout << it.m_key << " " << it.m_variable << std::endl;
    }
  }
*/

/*//! Example: Image usage. 
  Image *img = new Image("resources/sprites/widgets/widget_menu.png");
  img->useImage();
  img->getName();
*/

/*//! Example: Shader usage. 
  Shader *sha = new Shader("resources/shaders/rectangle.shader");
  sha->getName();
  sha->useShader();
*/

/*//! Example: Font usage:
  Font font("resources/fonts/04b_30.ttf", 15);
  font.getName();
  font.getFont();
*/

/*//! Example: TextObject usage.
  Primitive examplePrimitive;
  Text text;
  text.setFont("resources/fonts/04b_30.ttf", 15);
  text.setPrimitive(examplePrimitive);
  text.setText("Igor");

  testText = text;
*/

/*//! Example: Primitive usage.
  Primitive primitive;
  primitive.setColor(10.0f);
  primitive.setPosition(10.0f);
  primitive.setSize(10.0f);
  primitive.getColor();
  primitive.getPosition();
  primitive.getSize();
*/

//! Example: PrimitiveRenderer usage.
  PrimitiveRenderer::initRenderer();

/*//! Example: TextRenderer usage.
  TextRenderer::init();
*/

//! Example: BaseObject usage.
  // testBaseObject.setImage("resources/sprites/widgets/widget_menu.png");
  // testBaseObject.setShader("resources/shaders/circle.shader");
  // testBaseObject.getPrimitive().setPosition(800.0f, -300.0f);
  // testBaseObject.getPrimitive().setSize(10.0f);
  // testBaseObject.getPrimitive().setColor(1.0f, 0.0f, 0.0f);
  // testBaseObject.setText("Igor");
  // testBaseObject.saveObject();

//CursorObject::getObject().setShader("resources/shaders/circle.shader");
//particleGenerator = new ParticleGenerator(&CursorObject::getObject(), ParticleEffect::FOLLOW);

particleGenerator.getGeneratorSettings().setColorRange(glm::vec3(1.0f, 0.0f, 0.0f));
particleGenerator.getGeneratorSettings().setPositionRange(glm::vec2(800.0f, -300.0f));
particleGenerator.getGeneratorSettings().setSizeRange(glm::vec2(800.0f, 600.0f));
particleGenerator.getGeneratorSettings().setShader("resources/shaders/circle.shader");
}

GLvoid StatePlayground::stateLogic() {
}

GLvoid StatePlayground::stateRender() {

particleGenerator.renderParticles();

/*//! Example: TextRenderer usage.  
  TextRenderer::renderText(testTextObject);
*/
}