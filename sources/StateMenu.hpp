/*
    StateMenu.hpp
    Purpose: Menu state

    @author Igor Sadza
    @version 0.1 - 07/03/19
*/
#ifndef STATE_MENU_HPP
#define STATE_MENU_HPP

#include "CommonIncludes.hpp"
#include "State.hpp"

class StateMenu : public State {
private:
public:
  StateMenu(const std::string &t_pathToStateSource);
  GLvoid stateLogic();
  GLvoid stateRender();
};

#endif // STATE_MENU_HPP