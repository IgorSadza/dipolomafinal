/*
    Text.hpp
    Purpose: Text object for render purpose.

    @author Igor Sadza
    @version 0.1 - 10/03/19
*/
#ifndef TEXT_HPP
#define TEXT_HPP

#include "CommonIncludes.hpp"
#include "Font.hpp"
#include "Primitive.hpp"

class Text {
    private:
        std::string     m_text;
        Font            m_font;
        glm::vec2       m_textPosition;        
        Primitive       *m_primitive;

        GLvoid          textCentering();

    public:
        Text();
        GLvoid          setText(const std::string t_text);
        GLvoid          setFont(const std::string &t_fontPath, GLuint t_fontSize);
        GLvoid          setPrimitive(Primitive &t_primitive);

        std::string     &getText();
        Font            &getFont();
        glm::vec2       &getPosition();
        Primitive       getPrimitive();
        
        GLvoid          renderText();

};

#endif // TEXT_HPP